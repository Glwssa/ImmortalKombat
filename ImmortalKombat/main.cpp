// ImmortalKombat.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "allegro5/allegro.h"
#include "allegro5/allegro_image.h"
#include "allegro5/allegro_native_dialog.h"
using namespace std;
const float FPS = 12;
const int SCREEN_W = 640;
const int SCREEN_H = 460;



enum MYKEYS {
	KEY_UP, KEY_DOWN, KEY_LEFT, KEY_RIGHT, KEY_M //M: punch key
};

enum PLAYER_STATE {
	ready, crouch, movement, low_punch
};

bool key[5] = { false, false, false, false, false };
bool redraw = true;
bool doexit = false;

//STRUCT TOU KATHE PAIXTI
struct player_t {
	int player_number;
	float pos_x;
	float pos_y;
	PLAYER_STATE player_state;
	ALLEGRO_BITMAP *player_bmp = NULL;
	float animate_ready;
	float animate_movement;
	float animate_crouch;
};

void Player_Draw(player_t *player) {
	if (player->player_state == ready) {
		al_draw_scaled_bitmap(player->player_bmp, player->animate_ready * 58.7, 0, 55, 120, player->pos_x, player->pos_y, 150, 250, 0);
	}
	if (player->player_state == movement) {
		al_draw_scaled_bitmap(player->player_bmp, player->animate_movement * 58.7, 130, 55, 120, player->pos_x, player->pos_y, 150, 250, 0);
	}
	if (player->player_state == crouch) {
		al_draw_scaled_bitmap(player->player_bmp, player->animate_crouch * 58.7, 0, 55, 120, player->pos_x, player->pos_y, 150, 250, 0);
		//1233
	}
	if (player->player_state == low_punch) {
		al_draw_scaled_bitmap(player->player_bmp, player->animate_low_punch * 58.7, 360, 55, 120, player->pos_x, player->pos_y, 150, 250, 0);
	}
}
////////85695687

void Event_Handler(player_t *player, ALLEGRO_EVENT ev) {
	if (ev.type == ALLEGRO_EVENT_TIMER) {// des ama einai na spasoume auta ta megala if se mikroteres functions ( sta edw shmeia)
		player->player_state = ready;
		if (player->player_state == ready && player->animate_ready < 7) {
			player->animate_ready++;
		}
		if (player->player_state == ready && player->animate_ready >= 7) {
			player->animate_ready = 1.45;
		}
		if (key[KEY_UP] && player->pos_y >= 4.0) {
			player->pos_y -= 0;
		}

		if (key[KEY_DOWN] && player->pos_y <= SCREEN_H) {
			player->player_state = crouch;
			if (player->animate_crouch < 17) {
				player->animate_crouch++;
			}
			if (player->animate_crouch == 17.5) {
				player->animate_crouch = 19.1;
			}
			player->pos_y += 0;
		}

		if (key[KEY_LEFT] && player->pos_x >= 4.0) {
			player->player_state = movement;
			if (player->animate_movement > 8) {
				player->animate_movement = 0.112;
			}
			player->animate_movement++;
			player->pos_x -= 20.0;
		}

		if (key[KEY_RIGHT] && player->pos_x <= SCREEN_W) {
			player->player_state = movement;
			if (player->animate_movement > 8) {
				player->animate_movement = 0.112;
			}
			player->animate_movement++;
			player->pos_x += 20.0;
		}

		if (key[KEY_M] && player->pos_x <= SCREEN_W) {
			player->player_state = low_punch;
			if (player->animate_low_punch == 0.15) {
				player->animate_low_punch = 1.15;
			}
			else if (player->animate_low_punch == 1.15) {
				player->animate_low_punch = 2.15;
			}
			else if (player->animate_low_punch >= 2.15 && player->animate_low_punch < 4.15) {
				player->animate_low_punch++;
			}
			else if (player->animate_low_punch == 4.15) {
				player->animate_low_punch = 0.15;
			}
		}

		redraw = true;
	}
	else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {// edw
		//break;
	}
	else if (ev.type == ALLEGRO_EVENT_KEY_DOWN) {//edw
		switch (ev.keyboard.keycode) {
		case ALLEGRO_KEY_UP:
			key[KEY_UP] = true;
			break;

		case ALLEGRO_KEY_DOWN:
			key[KEY_DOWN] = true;
			break;

		case ALLEGRO_KEY_LEFT:
			key[KEY_LEFT] = true;
			break;

		case ALLEGRO_KEY_RIGHT:
			key[KEY_RIGHT] = true;
			break;

		case ALLEGRO_KEY_M:
			key[KEY_M] = true;
			player->animate_low_punch = 0.15;
			break;
		}
	}
	else if (ev.type == ALLEGRO_EVENT_KEY_UP) {//edw
		switch (ev.keyboard.keycode) {
		case ALLEGRO_KEY_UP:
			key[KEY_UP] = false;
			break;

		case ALLEGRO_KEY_DOWN:
			player->animate_crouch = 16.5;
			key[KEY_DOWN] = false;
			break;

		case ALLEGRO_KEY_LEFT:
			player->animate_movement = 0.15;
			key[KEY_LEFT] = false;
			break;

		case ALLEGRO_KEY_RIGHT:
			player->animate_movement = 0.15;
			key[KEY_RIGHT] = false;
			break;

		case ALLEGRO_KEY_M:
			key[KEY_M] = false;
			break;

		case ALLEGRO_KEY_ESCAPE:
			doexit = true;
			break;
		}
	}
}

int main(int argc, char **argv)
{
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_BITMAP *arena = NULL;

	//ORISMOS KAI INIT TOU PLAYER1
	player_t aplayer;
	player_t *player1;
	player1 = &aplayer;

	player1->player_number = 1;
	player1->pos_x = 0;
	player1->pos_y = SCREEN_H / 2.2;
	player1->player_state = ready;
	player1->animate_ready = 1.45;
	player1->animate_movement = 0.15;
	player1->animate_crouch = 16.5;





	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	if (!al_install_keyboard()) {
		fprintf(stderr, "failed to initialize the keyboard!\n");
		return -1;
	}

	timer = al_create_timer(1.0 / FPS);
	if (!timer) {
		fprintf(stderr, "failed to create timer!\n");
		return -1;
	}

	display = al_create_display(SCREEN_W, SCREEN_H);
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		al_destroy_timer(timer);
		return -1;
	}

	if (!al_init_image_addon()) {
		al_show_native_message_box(display, "Error", "Error", "Failed to initialize al_init_image_addon!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		return 0;
	}

	arena = al_load_bitmap("arena.jpg");
	player1->player_bmp = al_load_bitmap("scorpion.png");


	if (!player1->player_bmp) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load image!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		al_destroy_display(display);
		return 0;
	}

	if (!arena) {
		al_show_native_message_box(display, "Error", "Error", "Failed to load image!",
			NULL, ALLEGRO_MESSAGEBOX_ERROR);
		al_destroy_display(display);
		return 0;
	}
	//al_convert_mask_to_alpha(player1, al_map_rgb(0, 0, 0));


	al_set_target_bitmap(player1->player_bmp);

	//al_clear_to_color(al_map_rgb(255, 255, 255));

	al_set_target_bitmap(al_get_backbuffer(display));

	event_queue = al_create_event_queue();
	if (!event_queue) {
		fprintf(stderr, "failed to create event_queue!\n");
		al_destroy_bitmap(player1->player_bmp);
		al_destroy_bitmap(arena);
		al_destroy_display(display);
		al_destroy_timer(timer);
		return -1;
	}

	al_register_event_source(event_queue, al_get_display_event_source(display));

	al_register_event_source(event_queue, al_get_timer_event_source(timer));

	al_register_event_source(event_queue, al_get_keyboard_event_source());

	al_clear_to_color(al_map_rgb(0, 0, 0));

	al_flip_display();

	al_start_timer(timer);

	while (!doexit)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);
		//COUNTERS GIA TO ANIMATION KAI MOVEMENT EVENT HANDLER SUNARTISH ME TA EVENT SUMFWNA TO TIMER CALL H BUTTON
		Event_Handler(player1, ev);

		if (redraw && al_is_event_queue_empty(event_queue)) {
			redraw = false;

			al_clear_to_color(al_map_rgb(0, 0, 0));

			al_draw_bitmap(arena, 0, 0, 0);
			//EMFANISH XARAKTHRWN
			Player_Draw(player1);


			al_flip_display();
		}
	}

	al_destroy_bitmap(player1->player_bmp);
	al_destroy_bitmap(arena);
	al_destroy_timer(timer);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);

	return 0;
}
// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
